const connect = require("../db/connect");

module.exports = class scheduleController {
  static async createSchedule(req, res) {
    const { dateStart, dateEnd, days, user, classroom, timeStart, timeEnd } =
      req.body;

    if (
      !dateStart ||
      !dateEnd ||
      !days ||
      !user ||
      !classroom ||
      !timeStart ||
      !timeEnd
    ) {
      return res.status(400).json({ error: "Preencha todos os campos" });
    };  // Fim do if

    const daysString = days.map((day) => `$(day)`).join(", ");

    // Verificando se já não existe uma reserva
    try {
      const overlapQuery = `SELECT * FROM  schedule
            WHERE classroom = '${classroom}'
            AND (
                (dateStart <= '${dateEnd}' AND dateEnd >= '${dateStart}')
            )  AND (
                (timeStart <= '${timeEnd}' AND timeEnd >= '${timeStart}')
            ) AND (
                (days LIKE '%Seg%' AND '${daysString}' LIKE '%Seg%')OR
                (days LIKE '%Ter%' AND '${daysString}' LIKE '%Ter%')OR
                (days LIKE '%Qua%' AND '${daysString}' LIKE '%Qua%')OR
                (days LIKE '%Qui%' AND '${daysString}' LIKE '%Qui%')OR
                (days LIKE '%Sex%' AND '${daysString}' LIKE '%Sex%')OR
                (days LIKE '%Sab%' AND '${daysString}' LIKE '%Sab%')

            )
            `;

      connect.query(overlapQuery, function (err, results) {
        if (err) {
          return res
            .status(500)
            .json({ error: "Erro ao verificar agendamento existente" });
        };  // Fim do if da connect.query

        // Se houver resultado a consulta, já exite agendamento
        if (results.length > 0) {
          return res
            .status(400)
            .json({ error: "Sala ocupada para os mesmos dias e horários" });
        };  // Fim do if de sala ocupada

        // Caso a query não retorne nada inserimos na tabela
        const insertQuery = `INSERT INTO schedule (dateStart,
                                      dateEnd, 
                                      days, 
                                      user, 
                                      classroom,
                                      timeStart, 
                                      timeEnd)
                                      VALUES(
                                              '${dateStart}',
                                              '${dateEnd}',
                                              '${daysString}',
                                              '${user}',
                                              '${classroom}',
                                              '${timeStart}',
                                              '${timeEnd}'
                                      )
                                    `;

        // Executando a query de inserção
        connect.query(insertQuery, function (err) {
            if (err) {
                console.error(err);
                return res.status(500).json({error: "Erro ao cadastrar agendamento"});
            }  // Fim do if
            return res.status(201).json({ message: "Agendamento cadastrado com sucesso"});
        });   // Fim da connect.quey de inserção
      
    });  // Fim da connect.query
    
    } catch (error) {

        console.error("Erro ao executar a consulta:", error);
        res.status(500).json({error: "Erro interno no servidor"});

    };  // Fim do catch
  };  // Fim da createSchedule
};  // Fim da class
